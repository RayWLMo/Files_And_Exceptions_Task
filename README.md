# Files and Exceptions Task

## Task Description
- Able to read a file
- Able to edit the file
- Doesn't show error to user and be able to handle the error
- Use functions and classes to simplify the code
- Turn the code into a python package for anyone to use

## Task Solution
### order_edit.py
```python
class FileManagement:  # Creating FileManagement class

    def Add_To_File(self):  # Creating the amending function to add to order
        try:
            new_item = str(input("What would you like to add to the order?    "))
            with open(self,"a") as file:
                file.write(f"\n{new_item}")
                print(f"{new_item} was added to the order successfully")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")

    def Read_File(self):  # Creating the reading function to exclusively see the order
        try:
            with open(self, "r") as file:
                order = file.read()
                print(f"Your order is:\n{order}")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")
```
### program.py
```python
from app.order_edit import FileManagement  # Importing the FileManagement class from order_edit.py

user_prompt = True

file_name = input("What is the name of the file?    ")  # Asks the user for the file name

while user_prompt:  # while loop so the user can be constantly prompted
    user_choice = str(input("What would you to see or add to the order? (read/add/exit) "))
    if user_choice.lower() == "read":
        print(FileManagement.Read_File(f"{file_name}"))
    elif user_choice.lower() == "add":
        print(FileManagement.Add_To_File(f"{file_name}"))
    elif user_choice.lower() == "exit":
        print("Thank you for using this program")
        user_prompt = False
    else:
        print(f"{user_choice} is an invalid option. Please type one of the inputs provided")
```