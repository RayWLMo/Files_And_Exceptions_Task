
class FileManagement:  # Creating FileManagement class

    def Add_To_File(self):  # Creating the amending function to add to order
        try:
            new_item = str(input("What would you like to add to the order?    "))
            with open(self,"a") as file:
                file.write(f"\n{new_item}")
                print(f"{new_item} was added to the order successfully")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")

    def Read_File(self):  # Creating the reading function to exclusively see the order
        try:
            with open(self, "r") as file:
                order = file.read()
                print(f"Your order is:\n{order}")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")