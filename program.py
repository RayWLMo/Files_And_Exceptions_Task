from app.order_edit import FileManagement  # Importing the FileManagement class from order_edit.py

user_prompt = True

file_name = input("What is the name of the file?    ")  # Asks the user for the file name

while user_prompt:  # while loop so the user can be constantly prompted
    user_choice = str(input("What would you to see or add to the order? (read/add/exit) "))
    if user_choice.lower() == "read":
        print(FileManagement.Read_File(f"{file_name}"))
    elif user_choice.lower() == "add":
        print(FileManagement.Add_To_File(f"{file_name}"))
    elif user_choice.lower() == "exit":
        print("Thank you for using this program")
        user_prompt = False
    else:
        print(f"{user_choice} is an invalid option. Please type one of the inputs provided")

